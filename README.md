# ElementLinks in ATLAS `DAOD_PHYSLITE` with uproot and awkward array

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.cern.ch%2Fnihartma%2Fawkward-elementlinks.git/HEAD)

To run locally, install the packages from `requirements.txt`
