import numpy as np
import awkward
from numbers import Number


def read_vector_string(data, start=0):
    start = start + 6
    # the last 4 bytes of the 10-byte vector header tell us the size of the vector
    vector_size = np.frombuffer(data[start : start + 4].tobytes(), dtype=">i4")[0]
    pos = start + 4
    strings = []
    for _ in range(vector_size):
        # the first byte of one vector element tells us the length of the string
        string_len = data[pos]
        pos += 1
        # the rest is then just the string :)
        strings.append(data[pos : pos + string_len].tobytes().decode())
        pos += string_len
    return strings, pos


def get_target_offsets(offsets, event_index):
    """
    Adapted from coffea.nanoevents.methods.physlite

    https://github.com/CoffeaTeam/coffea/blob/b303b7d150c4b54ca7e19afc690ba8b439a55d2f/coffea/nanoevents/methods/physlite.py#L67
    """
    if isinstance(event_index, Number):
        return offsets[event_index]

    def descend(layout, depth, **kwargs):
        if layout.purelist_depth == 1:
            return awkward.contents.NumpyArray(offsets)[layout]

    return awkward.transform(descend, event_index)


def get_global_index(target, eventindex, index):
    """
    Adapted from coffea.nanoevents.methods.physlite

    https://github.com/CoffeaTeam/coffea/blob/b303b7d150c4b54ca7e19afc690ba8b439a55d2f/coffea/nanoevents/methods/physlite.py#L67
    """
    column = target[target.fields[0]]
    target_offsets = get_target_offsets(column.layout.offsets, eventindex)
    return target_offsets + index


def apply_global_index(array, index):
    """
    Adapted from coffea.nanovents

    https://github.com/CoffeaTeam/coffea/blob/b303b7d150c4b54ca7e19afc690ba8b439a55d2f/coffea/nanoevents/methods/base.py#L204.
    """

    # seems flatten with axis=None also flattens records
    # (which is not what we want here)
    flat_array = array
    for i in range(array.ndim - 1):
        flat_array = awkward.flatten(array)
    flat_array = awkward.to_layout(flat_array)

    if isinstance(index, int):
        # special case: link to single element or record
        out = flat_array[index]
        if isinstance(out, awkward.record.Record):
            out = awkward.Record(out, behavior=array.behavior)
        return out

    def flat_take(layout):
        idx = awkward.Array(layout)
        return flat_array[idx.mask[idx >= 0]]

    def descend(layout, depth, **kwargs):
        if layout.purelist_depth == 1:
            return flat_take(layout)

    index = awkward.broadcast_arrays(index)[0].layout
    return awkward.Array(
        awkward.transform(descend, index, highlevel=False), behavior=array.behavior
    )
